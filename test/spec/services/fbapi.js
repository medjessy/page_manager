'use strict';

describe('Service: fbAPI', function () {
    var FB = {
    api: function(url, mehtod, params, callback){
      return callback();
    }
  };

  // load the service's module
  beforeEach(module('pageManagerApp'));

  // instantiate service
  var fbAPI;
  beforeEach(inject(function (_fbAPI_) {
    fbAPI = _fbAPI_;
  }));


  it('fbAPI service should not be null', function () {
    expect(!!fbAPI).toBe(true);
  });
  it('fbAPI getPage should exist', function(){
    expect(fbAPI.getPage()).toBeDefined();
  });

});
