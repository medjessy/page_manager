'use strict';

angular
  .module('pageManagerApp', [
    'ngMaterial',
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ui.router'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: "/",
        templateUrl: "views/login.html",
        controller: 'LoginCtrl'
      })
      .state('page', {
        url: "/page",
        templateUrl: "views/page.html",
        controller: 'PageCtrl',
        abstract: true,
        resolve: {
          Auth: function (fbAPI) {
            return fbAPI.getLoginStatus();
          },
          Page: function(fbAPI){
            return fbAPI.getPage();
          },
          Picture: function(fbAPI){
            return fbAPI.getPicture();
          }
        }
      })
      .state('page.posts', {
        url: '/posts',
        templateUrl: "views/posts.html",
        controller: 'PostsCtrl',
        resolve: {
          PublishedPosts: function(fbAPI){
            return fbAPI.getPosts();
          },
          UnpublishedPosts: function(fbAPI){
            return fbAPI.getPosts(false);
          }
        },
        data: {
          header: "Posts"
        }
      });
    $urlRouterProvider.otherwise("/");
  })
  .run(function ($rootScope, $state, $cookies) {
    $rootScope.$on('$stateChangeError',
      function (event, toState, toParams, fromState, fromParams, error) {
        if (error.code == 190) {
          $cookies.remove("page_access");
          $cookies.remove("user_access");
          $state.go('login');
        }
      });
  })
  .constant("PAGE_ID", "1037154359704957");


window.fbAsyncInit = function () {
  FB.init({
    appId: '246014725791321',
    status: true,
    cookie: true,
    xfbml: false,
    version: 'v2.4'
  });
  angular.bootstrap(document, ['pageManagerApp'] );
};

(function (d) {
  var js,
    id = 'facebook-jssdk',
    ref = d.getElementsByTagName('script')[0];

  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement('script');
  js.id = id;
  // js.async = true;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  ref.parentNode.insertBefore(js, ref);

} (document));
