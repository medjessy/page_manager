'use strict';

angular.module('pageManagerApp')
    .service('fbAPI', function ($q, PAGE_ID, $cookies) {

        this.login = function () {
            var deferred = $q.defer();
            FB.login(function (response) {
                if (response.status === 'connected') {
                    deferred.resolve(response);
                } else if (response.status === 'not_authorized') {
                    deferred.reject("Error: " + response.status);
                } else {
                    deferred.reject("Error: " + response.status);
                }
            }, { scope: 'manage_pages,publish_pages,read_insights' });
            return deferred.promise;
        };

        this.accounts = function () {
            return facebook("/" + PAGE_ID, "GET", { "fields": "access_token" });
        };

        this.getLoginStatus = function () {
            var deferred = $q.defer();
            FB.getLoginStatus(function (response) {
                if (response.status == 'connected') {
                    deferred.resolve(response);
                } else {
                    deferred.reject("LOGIN_ERROR");
                }
            }, true);
            return deferred.promise;
        };

        this.getPage = function () {
            var params = {
                "access_token": $cookies.get("page_access")
            }
            return facebook("/" + PAGE_ID, "GET", params);
        };

        this.getPosts = function (is_published) {
            var p = (is_published == undefined) ? true : is_published;
            var params = {
                "fields": "message,story,insights{name,values}",
                "is_published": p,
                "access_token": $cookies.get("page_access")
            }
            return facebook("/" + PAGE_ID + "/promotable_posts", "GET", params);
        };

        this.addNewPost = function (data) {
            var params = {
                "message": data.message,
                "published": data.published,
                "access_token": $cookies.get("page_access")
            };
            return facebook("/" + PAGE_ID + "/feed", "POST", params);
        };

        this.deletePost = function (id) {
            var params = {
                "access_token": $cookies.get("page_access")
            }
            return facebook("/" + id, "DELETE", params);
        };

        this.publish = function (id) {
            var params = {
                "is_published": true,
                "access_token": $cookies.get("page_access")
            };
            return facebook("/" + id, "POST", params);
        };

        this.getPostInsights = function (id) {
            var params = {
                "access_token": $cookies.get("page_access")
            };
            return facebook("/" + id + "/insights/post_impressions", "GET", params);
        };

        this.getPicture = function (type) {
            var t = !type ? "normal" : type;
            var params = {
                "type": t,
                "access_token": $cookies.get("page_access")
            };
            return facebook("/" + PAGE_ID + "/picture", "GET", params);
        };

        var facebook = function (url, method, params) {
            var deferred = $q.defer();
            // console.log(url +"," + method + "," + JSON.stringify(params));
            FB.api(
                url, method, params,
                function (response) {
                    if (response && !response.error) {
                        deferred.resolve(response);
                    } else {
                        console.log(response.error);
                        deferred.reject(response.error);
                    }
                }
            );
            return deferred.promise;
        };
    });



