'use strict';

angular.module('pageManagerApp')
  .service('Toast', function ($mdToast) {

    this.show = function (message) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(message)
          .position('top right')
          .hideDelay(3000)
      );
    };
    
  });