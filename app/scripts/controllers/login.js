'use strict';

angular.module('pageManagerApp')
  .controller('LoginCtrl', function ($scope, $rootScope, $state, fbAPI, Toast, $cookies, PAGE_ID) {

    $scope.login = function () {
      fbAPI.login().then(function (response) {
        $cookies.put("user_access", response.authResponse.accessToken);
        fbAPI.accounts().then(function (response) {
          $cookies.put("page_access", response.access_token);
          $state.go('page.posts');
        }, function (error) {
          Toast.show(error.message);
        });

      }, function (error) {
        Toast.show(error.message);
      });
    };
  });

