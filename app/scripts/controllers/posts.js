'use strict';

angular.module('pageManagerApp')
  .controller('PostsCtrl', function ($scope, $rootScope, fbAPI, PublishedPosts, UnpublishedPosts, Toast, $mdDialog) {

    $scope.published_posts = PublishedPosts.data;
    $scope.unpublished_posts = UnpublishedPosts.data;

    console.log($scope.published_posts);
    
    $scope.published = "true";

    $scope.addNewPost = function () {
      if($scope.newPost == undefined || $scope.newPost == ""){
        Toast.show("Post cannot be empty");
        return;
      }
      var data = {
        message: $scope.newPost,
        published: $scope.published == "true" ? true : false
      };
      fbAPI.addNewPost(data).then(function (response) {
        $scope.newPost = "";
        Toast.show("Posted successfully");
        refresh();
      }, function (error) {
        Toast.show(error.message);
      });
    };

    $scope.confirmDelete = function (event, id) {
      var confirm = $mdDialog.confirm()
        .title("Are you sure you want to delete this post?")
        .textContent('')
        .ariaLabel('Confirm delete post')
        .targetEvent(event)
        .ok('YES')
        .cancel('NO');
      $mdDialog.show(confirm).then(function () {
        deletePost(id);
      }, function () {
      });
    };

    var deletePost = function (id) {
      fbAPI.deletePost(id).then(function (response) {
        Toast.show("Post deleted");
        refresh();
      }, function (error) {
        Toast.show(error.message);
      });
    };

    $scope.publish = function(id){
      fbAPI.publish(id).then(function(response){
        Toast.show("Post published successfully");
        refresh();
      }, function(error){
        Toast.show(error.message);
      });
    };

    var refresh = function(){
        fbAPI.getPosts().then(function (response) {
          $scope.published_posts = response.data;
        });
        fbAPI.getPosts(false).then(function (response) {
          $scope.unpublished_posts = response.data;
        });
    };

    $scope.totalViews = function(data){
      for(var i=0;i<data;i++){
          console.log(data[i]);
        if(data[i].name == "post_impressions"){
          console.log(data[i]);
          return data[i].values[0].value;
        }
      }
    };
  });

