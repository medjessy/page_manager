'use strict';

angular.module('pageManagerApp')
  .controller('PageCtrl', function ($scope, $rootScope, fbAPI, $state, Page, Picture, $cookies) {
    if($cookies.get("page_access") == undefined || $cookies.get("user_access") == undefined){
      $state.go("login");      
    }
    
    $scope.header = $state.current.data.header;
    $scope.page_name = Page.name;
    $rootScope.picture = Picture.data.url;
    
    $scope.logout = function(){
      FB.logout(function(response){
        if(response && !response.error){
          $cookies.remove("user_access");
          $cookies.remove("page_access");
          $state.go("login");
        }
      });
    };

  });

